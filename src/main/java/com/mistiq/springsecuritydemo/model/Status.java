package com.mistiq.springsecuritydemo.model;

public enum Status {
    ACTIVE,
    BANNED;
}
